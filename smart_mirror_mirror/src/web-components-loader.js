import WeatherComponent from "./components/weather/index.js"
import DayHour from "./components/dayhour/index.js"

const QRCode = require('easyqrcodejs');


class App{
    constructor(){
        var serverIP = "35.232.167.89"
        fetch("http://"+serverIP+":8000/uuid")
            .then((response)=>{
                return response.json()
            })
            .then((jsonBody)=>{
                var uuid = jsonBody.uuid
                var W3CWebSocket = require('websocket').w3cwebsocket;
                var client = new W3CWebSocket('ws://'+serverIP+':8080/'+uuid, 'echo-protocol');
                
                client.onerror = function() {
                    console.log('Connection Error');
                };
                
                client.onopen = function() {
                    console.log('WebSocket Client Connected');
                    client.send("ack " + uuid+"  :)");
                };
                
                client.onclose = function() {
                    console.log('echo-protocol Client Closed');
                };
                
                client.onmessage = function(e) {
                    if (typeof e.data === 'string') {
                        console.log("Received: '" + e.data + "'");
                        if (e.data == "QR Scanned") {
                            client.send("close")
                            document.getElementById("welcome").style.display = "none"
                            document.getElementById("app").style.display = "grid"
                        }
                    }
                };
                //docu: https://www.npmjs.com/package/easyqrcodejs
                var options_object = {
                    // ====== Basic
                    text: uuid,
                    width: 256,
                    height: 256,
                    colorDark : "#000000",
                    colorLight : "#ffffff",
                    correctLevel : QRCode.CorrectLevel.H, // L, M, Q, H
                    dotScale: 1,
                    quietZone: 1,
                    quietZoneColor: '#ffffff',
                }
                var qrCodeElem = document.getElementById("qrCode")
                console.log(document, qrCodeElem);
                var qrcode = new QRCode(qrCodeElem, options_object);
            })
    }
}

window.onload = ()=>{
    var app = new App()
}
