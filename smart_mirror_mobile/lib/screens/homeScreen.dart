import 'package:flutter/material.dart';
import 'package:smart_mirror_mobile/screens/qrScannerScreen.dart';

class HomeScreen extends StatefulWidget {
    @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  bool _ = false;

  initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
          appBar: AppBar(
            title: Text('Welcome to Flutter'),
          ),
          body: FunctionalitiesList(),
      ),
    );
  }
}

class FunctionalitiesListState extends State<FunctionalitiesList> {
  final _functionalities = [{"name":"Profile", "icon": Icon(Icons.person_pin, color:Colors.blue), "screen": QrScannerScreen()}, {"name":"Google Calendar", "icon": Icon(Icons.calendar_today, color:Colors.blue), "screen": QrScannerScreen()}];
  final _biggerFont = const TextStyle(fontSize: 18.0);

  Widget build(BuildContext context) {
    return ListView.separated(
        padding: const EdgeInsets.all(16.0),
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: _functionalities.length,
        itemBuilder: /*1*/ (context, i) {
          return Material(
            child: InkWell(
              onTap: () {}, // needed
              child: ListTile(
                  title: Text(
                    _functionalities[i]["name"].toString(),
                    style: _biggerFont,
                  ),
                  trailing: _functionalities[i]["icon"],
                  onTap: ()=>{
                    Navigator.of(context).push(
                      MaterialPageRoute<void>(
                        builder: (BuildContext context) {
                          return _functionalities[i]["screen"];
                        }
                      )
                    ),
                  },
                ),
              ),
            );
          });
  }
}


class FunctionalitiesList extends StatefulWidget {
  @override
  FunctionalitiesListState createState() => new FunctionalitiesListState();
}

